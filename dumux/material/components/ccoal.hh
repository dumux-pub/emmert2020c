 /*
 * CCoal.hh
 *
 *  Created on: 24.10.2016
 *      Author: scholz
 */

/*!
 * \file
 * \ingroup Components
 * \brief A class for the Convertible Coal fluid properties
 */
#ifndef DUMUX_CCOAL_HH
#define DUMUX_CCOAL_HH

#include <dumux/material/components/base.hh>
#include <dumux/material/components/solid.hh>

namespace Dumux {
namespace Components {

/*!
 * \ingroup Components
 * \brief A class for the Convertible Coal properties
 */
template <class Scalar>
class CCoal
: public Components::Base<Scalar, CCoal<Scalar> >
, public Components::Solid<Scalar, CCoal<Scalar> >
{
public:
   /*!
    * \brief A human readable name for the Convertible Coal.
    */
    static std::string name()
    { return "Convertible Coal"; }

   /*!
    * \brief Returns true if the solid phase is assumed to be compressible
    */
    static constexpr bool solidIsCompressible()
    {
        return false; // iso c++ requires a return statement for constexpr functions
    }

   /*!
    * \brief The mass in [kg] of one mole of Convertible Coal.
    */
    static Scalar molarMass()
    { return 0.18016; } // kg/mol

   /*!
    * \brief The mass density \f$\mathrm{[kg/m^3]}\f$ of Convertible Coal.
    */
    static Scalar solidDensity(Scalar temperature)
    { return 1250; } // between 1100 to 1800 kg/m3,
    // Katie measured 1250 kg/m3 for batch experiment,
    // Elliot field measurements range from 1440 to 1540 kg/m3

};

} // end namespace Components
} // end namespace Dumux

#endif
