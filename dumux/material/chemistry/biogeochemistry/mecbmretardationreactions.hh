// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class. The chemical functions and derivations are implemented in the private part of
 * class.
 */
#ifndef DUMUX_MECBM_RETARDATION_REACTIONS_HH
#define DUMUX_MECBM_RETARDATION_REACTIONS_HH

namespace Dumux
{
/*!
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class. The chemical functions and derivations are implemented in the private part of
 * class.
 */
template <class TypeTag>
class MECBMRetardationReactionsChemistry
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ThisType = MECBMRetardationReactionsChemistry<TypeTag>;
    using Sources = GetPropType<TypeTag, Properties::NumEqVector>;
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    using VelocityVector = typename VelocityOutput::VelocityVector;
public:

    // bekommt Lösungsvektor des Tracers, speichert in privater Variable
    MECBMRetardationReactionsChemistry()
    {
        //growth and decay rate coefficients
        muCoalBacCoal_  = getParamFromGroup<Scalar>("BioCoefficients","muCoalBacC");   // Maximum specific growth rate for Primary Bacteria on Coal
        muAmCoalBacCoal_= getParamFromGroup<Scalar>("BioCoefficients","muAmCoalBacC"); // Maximum specific growth rate for Secondary Bacteria on Coal
        muAmCoalBacAm_  = getParamFromGroup<Scalar>("BioCoefficients","muAmCoalBacAm");// Maximum specific growth rate for Secondary Bacteria on Amendment
        muAcetoArch_    = getParamFromGroup<Scalar>("BioCoefficients","muAcetoArch");  // Maximum specific growth rate for Acetoclastic Archaea
        muHydroArch_    = getParamFromGroup<Scalar>("BioCoefficients","muHydroArch");  // Maximum specific growth rate for Hydrogenotrophic Archaea
        muMethyArch_    = getParamFromGroup<Scalar>("BioCoefficients","muMethyArch");  // Maximum specific growth rate for Methylotrophic Archaea
        Kbac_              = getParamFromGroup<Scalar>("BioCoefficients","Kbac");            // Decay Rate for Microbes
        Karch_              = getParamFromGroup<Scalar>("BioCoefficients","Karch");            // Decay Rate for Microbes

        //Monod half saturation constants
        Kc_     = getParamFromGroup<Scalar>("BioCoefficients","Kc");           // Monod Half saturation Constant for Coal
        KH2_    = getParamFromGroup<Scalar>("BioCoefficients","KH2");          // Monod Half saturation Constant for Hydrogen
        KAm_    = getParamFromGroup<Scalar>("BioCoefficients","KAm");          // Monod Half saturation Constant for Amendment
        KAc_    = getParamFromGroup<Scalar>("BioCoefficients","KAc");          // Monod Half saturation Constant for Acetate
        KCH3_   = getParamFromGroup<Scalar>("BioCoefficients","KCH3");         // Monod Half saturation Constant for Methyl

        //Factors
        FAcCoal_   = getParamFromGroup<Scalar>("BioCoefficients","FAcCoal",1);    // Factor for coal consuming bacteria on coal producing acetate
        FH2Coal_   = getParamFromGroup<Scalar>("BioCoefficients","FH2Coal",1);    // Factor for coal consuming bacteria on coal producing H2
        FCH3Coal_  = getParamFromGroup<Scalar>("BioCoefficients","FCH3Coal",1);    // Factor for coal consuming bacteria on coal producing CH3
        FAcAm_     = getParamFromGroup<Scalar>("BioCoefficients","FAcAm",1);    // Factor for amendment consuming bacteria on amendment producing acetate
        FH2Am_     = getParamFromGroup<Scalar>("BioCoefficients","FH2Am",1);    // Factor for amendment consuming bacteria on amendment producing acetate
        FCH3Am_    = getParamFromGroup<Scalar>("BioCoefficients","FCH3Am",1);    // Factor for amendment consuming bacteria on amendment producing acetate
        FCH4Ac_    = getParamFromGroup<Scalar>("BioCoefficients","FCH4Ac",1);    // Factor for acetoclastic archaea producing CH4/CO2
        FCH4H2_    = getParamFromGroup<Scalar>("BioCoefficients","FCH4H2",1);    // Factor for hydrogenotrophic archaea producing CH4/CO2
        FCH4CH3_   = getParamFromGroup<Scalar>("BioCoefficients","FCH4CH3",1);    // Factor for methyloclastic archaea producing CH4/CO2

        //Yields
        YccbcAc_   = getParamFromGroup<Scalar>("BioCoefficients","YCoalBacCAc")*(2-FAcCoal_);    // Yield of coal consuming Bacteria Biomass on Coal
        YccbcH2_   = getParamFromGroup<Scalar>("BioCoefficients","YCoalBacCH2")*(2-FH2Coal_);    // Yield of coal consuming Bacteria Biomass on Coal
        YacbcAc_   = YccbcAc_; // getParamFromGroup<Scalar>("BioCoefficients","YAmCoalBacCAc");  // Yield of coal & amendment consuming Bacteria on Coal
        YacbcH2_   = YccbcH2_; // getParamFromGroup<Scalar>("BioCoefficients","YAmCoalBacCH2");  // Yield of coal & amendment consuming Bacteria on Coal
        YacbamAc_  = getParamFromGroup<Scalar>("BioCoefficients","YAmCoalBacAmAc")*(2-FAcAm_); // Yield of coal & amendment consuming Bacteria on Amendment
        YacbamH2_  = getParamFromGroup<Scalar>("BioCoefficients","YAmCoalBacAmH2")*(2-FH2Am_); // Yield of coal & amendment consuming Bacteria on Amendment
        YacbamCH3_ = getParamFromGroup<Scalar>("BioCoefficients","YAmCoalBacAmCH3")*(2-FCH3Am_); // Yield of coal & amendment consuming Bacteria on Amendment
        amFac_  = getParamFromGroup<Scalar>("BioCoefficients","amFac", 1); // amendmentFactor

        YaaAc_   = getParamFromGroup<Scalar>("BioCoefficients","YAcetoArchAc")*(2-FCH4Ac_); // Yield of Acetoclastic Archaea Biomass on Acetate
        YhaH2_   = getParamFromGroup<Scalar>("BioCoefficients","YHydroArchH2")*(2-FCH4H2_); // Yield of Hydrogenotrophic Archaea Biomass on Hydrogen
        YmaCH3_  = getParamFromGroup<Scalar>("BioCoefficients","YMethyArchCH3")*(2-FCH4CH3_);// Yield of Methylotrophic Archaea Biomass on Methyl
        YAccc_   = getParamFromGroup<Scalar>("BioCoefficients","YAccc")*FAcCoal_;        // Yield of Acetate from Coal
        YH2cc_   = getParamFromGroup<Scalar>("BioCoefficients","YH2cc")*FH2Coal_;        // Yield of Hydrogen from Coal
        YCH3cc_  = getParamFromGroup<Scalar>("BioCoefficients","YCH3cc")*FCH3Coal_;        // Yield of Hydrogen from Coal
        YAcAm_   = getParamFromGroup<Scalar>("BioCoefficients","YAcAm")*FAcAm_;        // Yield of Acetate from Amendment
        YH2Am_   = getParamFromGroup<Scalar>("BioCoefficients","YH2Am")*FH2Am_;        // Yield of Hydrogen from Amendment
        YCH3Am_  = getParamFromGroup<Scalar>("BioCoefficients","YCH3Am")*FCH3Am_;       // Yield of Methyl from Amendment
        YCH4Ac_  = getParamFromGroup<Scalar>("BioCoefficients","YCH4Ac")*FCH4Ac_;       // Yield of CH4 from Acetate
        YCH4H2_  = getParamFromGroup<Scalar>("BioCoefficients","YCH4H2")*FCH4H2_;       // Yield of CH4 from Hydrogen
        YCH4CH3_ = getParamFromGroup<Scalar>("BioCoefficients","YCH4CH3")*FCH4CH3_;      // Yield of CH4 from Methyl
        YCO2Ac_  = getParamFromGroup<Scalar>("BioCoefficients","YCO2Ac")*FCH4Ac_;       // Yield of CO2 from Acetate
        YCO2H2_  = getParamFromGroup<Scalar>("BioCoefficients","YCO2H2")*FCH4H2_;       // Yield of CO2 from Acetate
        YCO2CH3_ = getParamFromGroup<Scalar>("BioCoefficients","YCO2CH3")*FCH4CH3_;       // Yield of CO2 from Methyl

        lambdaDepos_ = getParamFromGroup<Scalar>("Retardation","lambdaDepos");
        lambdaResusp_= getParamFromGroup<Scalar>("Retardation","lambdaResusp");
        grainDiameter_ = getParamFromGroup<Scalar>("Retardation","grainDiameter",0.001);

    }

    //Indices of the phases
    static const int phase0Idx    = FluidSystem::phase0Idx;
    static const int phase1Idx    = FluidSystem::phase1Idx;
    static const int CoalBacPhaseIdx   = SolidSystem::CoalBacPhaseIdx;
    static const int AmCoalBacPhaseIdx = SolidSystem::AmCoalBacPhaseIdx;
    static const int AcetoArchPhaseIdx = SolidSystem::AcetoArchPhaseIdx;
    static const int HydroArchPhaseIdx = SolidSystem::HydroArchPhaseIdx;
    static const int MethyArchPhaseIdx = SolidSystem::MethyArchPhaseIdx;
    static const int CCoalPhaseIdx     = SolidSystem::CCoalPhaseIdx;
    static const int SolidAmendmentPhaseIdx     = SolidSystem::SolidAmendmentPhaseIdx;

    //Indices of the components
    static constexpr int numComponents = FluidSystem::numComponents;

    static const int wCompIdx     = FluidSystem::wCompIdx;
    static const int nCompIdx     = FluidSystem::nCompIdx;
    static const int CH4Idx       = FluidSystem::CH4Idx;
    static const int AcetateIdx   = FluidSystem::AcetateIdx;
    static const int AmendmentIdx = FluidSystem::AmendmentIdx;
    static const int RMethylIdx   = FluidSystem::RMethylIdx;
    static const int HydrogenIdx  = FluidSystem::H2Idx;
    static const int TCIdx        = FluidSystem::CO2Idx;

    //Indices of the bio/coal volume fractions
    static const int CoalBacIdx   = CoalBacPhaseIdx + numComponents;
    static const int AmCoalBacIdx = AmCoalBacPhaseIdx + numComponents;
    static const int AcetoArchIdx = AcetoArchPhaseIdx + numComponents;
    static const int HydroArchIdx = HydroArchPhaseIdx + numComponents;
    static const int MethyArchIdx = MethyArchPhaseIdx + numComponents;
    static const int CCoalIdx     = CCoalPhaseIdx + numComponents;
    static const int SolidAmendmentIdx     = SolidAmendmentPhaseIdx + numComponents;


    // erstelle globale Routine reactionGlobal, die beide
    // Lösungsvektoren bekommt (Flow und Tracer) und in main
    // aufgerufen wird. Damit werden Quellterme für jeden Freiheitsgrad
    // ausgerechnet und gespeichert.
    // Die lokalen Routine reactionSourceFlow/Tracer greifen auf die berechneten Werte
    // zurück.

    // bekommt auch FVElementGeometry und scv
    // aufspalten in reactionSourceFlow und reactionSourceTracer
    void reactionSource(Sources &q,
                        const VolumeVariables &volVars,
                        const Scalar dt)
    {
        // hole Tracer-Komponenten aus dem Lösungsvektor mit fvGeom und scv

        //define and compute some parameters for simplicity:
        //Scalar porosity = volVars.porosity();
        //Scalar initialPorosity = volVars.initialPorosity();
        //Scalar Sw  =  volVars.saturation(phase0Idx);
        //Scalar temperature = volVars.temperature();

        //TODO adapt comments
        Scalar cAcetate = volVars.moleFraction(phase0Idx, AcetateIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AcetateIdx);    //[kg_suspended_Biomass/m³_waterphase]
        if(cAcetate < 0)
            cAcetate = 0;
        Scalar cCarAm   = volVars.moleFraction(phase0Idx, AmendmentIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(AmendmentIdx); //[kg_suspended_Biomass/m³_waterphase]
        if(cCarAm < 0)
            cCarAm = 0;
        Scalar cRMethyl = volVars.moleFraction(phase0Idx, RMethylIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(RMethylIdx);    //[kg_suspended_Biomass/m³_waterphase]
        if(cRMethyl < 0)
            cRMethyl = 0;
        Scalar cHydrogen = volVars.moleFraction(phase0Idx, HydrogenIdx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(HydrogenIdx); //[kg_suspended_Biomass/m³_waterphase]
        if(cHydrogen < 0)
            cHydrogen = 0;

        // Scalar cCH4w = volVars.moleFraction(phase0Idx, CH4Idx) * volVars.molarDensity(phase0Idx) * FluidSystem::molarMass(CH4Idx);
        // Scalar cCH4n = volVars.moleFraction(phase1Idx, CH4Idx) * volVars.molarDensity(phase1Idx) * FluidSystem::molarMass(CH4Idx);

        // std::cout << "molDensity wPhase " << volVars.molarDensity(phase0Idx) << std::endl;
        // std::cout << "cAmendment " << cCarAm << std::endl;
        // std::cout << "cAcetate " << cAcetate << std::endl;
        // std::cout << "molFrac Acetate " << volVars.moleFraction(phase0Idx, AcetateIdx) << std::endl;
        // std::cout << "cCarAm " << cCarAm << std::endl;
        // std::cout << "molFrac CarAm " << volVars.moleFraction(phase0Idx, AmendmentIdx) << std::endl;
        // std::cout << "cRMethyl " << cRMethyl << std::endl;
        // std::cout << "molFrac RMethylIdx " << volVars.moleFraction(phase0Idx, RMethylIdx) << std::endl;
        // std::cout << "cHydrogen " << cHydrogen << std::endl;
        // std::cout << "molFrac HydrogenIdx " << volVars.moleFraction(phase0Idx, HydrogenIdx) << std::endl;
        // std::cout << "cCH4wetting " << cCH4w << std::endl;
        // std::cout << "cCH4gas " << cCH4n << std::endl;


        //compute biomass growth coefficients and rate
        Scalar massCoalBac_   = volVars.solidVolumeFraction(CoalBacPhaseIdx)*volVars.solidComponentDensity(CoalBacPhaseIdx);     //[kg_pb_Biomass/m³_total] volumetric fraction of primary bacteria attached reversibly
        Scalar massAmCoalBac_ = volVars.solidVolumeFraction(AmCoalBacPhaseIdx)*volVars.solidComponentDensity(AmCoalBacPhaseIdx); //[kg_sb_Biomass/m³_total] volumetric fraction of secondary bacteria attached reversibly
        Scalar massAcetoArch_ = volVars.solidVolumeFraction(AcetoArchPhaseIdx)*volVars.solidComponentDensity(AcetoArchPhaseIdx); //[kg_aa_Biomass/m³_total] volumetric fraction of acetoclastic archaea attached reversibly
        Scalar massHydroArch_ = volVars.solidVolumeFraction(HydroArchPhaseIdx)*volVars.solidComponentDensity(HydroArchPhaseIdx); //[kg_ha_Biomass/m³_total] volumetric fraction of hydrogenotrophic archaea attached reversibly
        Scalar massMethyArch_ = volVars.solidVolumeFraction(MethyArchPhaseIdx)*volVars.solidComponentDensity(MethyArchPhaseIdx); //[kg_ma_Biomass/m³_total] volumetric fraction of methylotrophic archaea attached reversibly
        Scalar massCCoal_     = volVars.solidVolumeFraction(CCoalPhaseIdx)*volVars.solidComponentDensity(CCoalPhaseIdx);         //[kg_cc_Biomass/m³_total] volumetric fraction convertible coal attached reversibly
        // std::cout<<"massCCoal_ "<< massCCoal_ <<std::endl;
        if(massCCoal_ < 0)
          massCCoal_ = 0;

        // std::cout << "massCoalBac_ " << massCoalBac_ << std::endl;
        // std::cout << "massAmCoalBac_ " << massAmCoalBac_ << std::endl;
        // std::cout << "massAcetoArch_ " << massAcetoArch_ << std::endl;
        // std::cout << "massHydroArch_ " << massHydroArch_ << std::endl;
        // std::cout << "massMethyArch_ " << massMethyArch_ << std::endl;
        // std::cout << "massCCoal_ " << massCCoal_ << std::endl;

        Scalar rgCoalBac      = muCoalBacCoal_ * massCCoal_ / (Kc_ + massCCoal_)*massCoalBac_;      //growth rate primary bacteria
        Scalar rdCoalBac      = Kbac_ * massCoalBac_;                                                  //decay rate primary bacteria
        Scalar rgAmCoalBacCoal = muAmCoalBacCoal_ * massCCoal_ / (Kc_ + massCCoal_)*massAmCoalBac_;  //growth rate secondary bacteria on coal
        Scalar rgAmCoalBacAm  = muAmCoalBacAm_ * cCarAm / (KAm_ + cCarAm)*massAmCoalBac_;           //growth rate secondary bacteria on amendment
        Scalar rgAmCoalBac    = rgAmCoalBacCoal + rgAmCoalBacAm;                                  //growth rate secondary bacteria total
        Scalar rdAmCoalBac    = Kbac_ * massAmCoalBac_;                                                //decay rate secondary bacteria total
        Scalar rgAcetoArch    = muAcetoArch_ * cAcetate / (KAc_ + cAcetate)*massAcetoArch_;         //growth rate acetoclastic archaea
        Scalar rdAcetoArch    = Karch_ * massAcetoArch_;                                                //decay rate acetoclastic archaea
        Scalar rgHydroArch    = muHydroArch_ * cHydrogen / (KH2_ + cHydrogen)*massHydroArch_;        //growth rate hydrogenotrophic archaea
        Scalar rdHydroArch    = Karch_ * massHydroArch_;                                                //decay rate hydrogenotrophic archaea
        Scalar rgMethyArch    = muMethyArch_ * cRMethyl / (KCH3_ + cRMethyl)*massMethyArch_;        //growth rate methylotrophic archaea
        Scalar rdMethyArch    = Karch_ * massMethyArch_;                                                //decay rate methylotrophic archaea

        // std::cout << "muCoalBacCoal_ " << muCoalBacCoal_ << std::endl;
        // std::cout << "massCCoal_ " << massCCoal_ << std::endl;
        // std::cout << "Kc_ " << Kc_ << std::endl;
        // std::cout << "massCoalBac_ " << massCoalBac_ << std::endl;
        // std::cout << "rdCoalBac " << rdCoalBac << std::endl;
        // std::cout << "rgAmCoalBacCoal " << rgAmCoalBacCoal << std::endl;
        // std::cout << "muAmCoalBacCoal_ " << muAmCoalBacCoal_ << std::endl;
        // std::cout << "massCCoal_ " << massCCoal_ << std::endl;
        // std::cout << "Kc_ " << Kc_ << std::endl;
        // std::cout << "massAmCoalBac_ " << massAmCoalBac_ << std::endl;
        // std::cout << "rgAmCoalBacAm " << rgAmCoalBacAm << std::endl;
        // std::cout << "rgCoalBac " << rgCoalBac << std::endl;
        // std::cout << "rgAmCoalBac " << rgAmCoalBac << std::endl;
        // std::cout << "rdAmCoalBac " << rdAmCoalBac << std::endl;
        // std::cout << "rgAcetoArch " << rgAcetoArch << std::endl;
        // std::cout << "rdAcetoArch " << rdAcetoArch << std::endl;
        // std::cout << "rgHydroArch " << rgHydroArch << std::endl;
        // std::cout << "rdHydroArch " << rdHydroArch << std::endl;
        // std::cout << "rgMethyArch " << rgMethyArch << std::endl;
        // std::cout << "rdMethyArch " << rdMethyArch << std::endl;

        //Phase reactions in [kg/(m3*s)]
        //introducing *10 factor to match Irfan's Matlab porosity formulation
        q[CoalBacIdx]     = (rgCoalBac - rdCoalBac) / 86400 / SolidSystem::molarMass(CoalBacPhaseIdx);
        q[AmCoalBacIdx]   = (rgAmCoalBac - rdAmCoalBac) / 86400 / SolidSystem::molarMass(AmCoalBacPhaseIdx);
        q[AcetoArchIdx]   = (rgAcetoArch - rdAcetoArch) / 86400 / SolidSystem::molarMass(AcetoArchPhaseIdx);
        q[HydroArchIdx]   = (rgHydroArch - rdHydroArch) / 86400 / SolidSystem::molarMass(HydroArchPhaseIdx);
        q[MethyArchIdx]   = (rgMethyArch - rdMethyArch) / 86400 / SolidSystem::molarMass(MethyArchPhaseIdx);

        // std::cout << "q[CoalBacIdx] " << q[CoalBacIdx] << std::endl;
        // std::cout << "q[AmCoalBacIdx] " << q[AmCoalBacIdx] << std::endl;
        // std::cout << "q[AcetoArchIdx] " << q[AcetoArchIdx] << std::endl;
        // std::cout << "q[HydroArchIdx]  " << q[HydroArchIdx] << std::endl;
        // std::cout << "q[MethyArchIdx]  " << q[MethyArchIdx]  << std::endl;

        //Component reactions in mol/(m3*s)
        q[CH4Idx]     = (rgHydroArch * YCH4H2_ / YhaH2_
                         + rgAcetoArch * YCH4Ac_ / YaaAc_
                         + rgMethyArch * YCH4CH3_ / YmaCH3_)
                        /86400 / FluidSystem::molarMass(CH4Idx); //+ sorp; //TODO: add when above is solved/clear
        q[AcetateIdx] = ((rgAmCoalBacCoal * YAccc_ / YacbcAc_)
                         + (rgAmCoalBacAm * YAcAm_ / YacbamAc_)
                         + (rgCoalBac * YAccc_ / YccbcAc_)
                         - rgAcetoArch / YaaAc_)
                        /86400  / FluidSystem::molarMass(AcetateIdx);
        q[AmendmentIdx]= -(rgAmCoalBacAm / YacbamAc_ + rgAmCoalBacAm / YacbamH2_ + rgAmCoalBacAm / YacbamCH3_ )
                         /86400 / FluidSystem::molarMass(AmendmentIdx);
        q[RMethylIdx] = ((rgAmCoalBacAm * YCH3Am_ / YacbamCH3_)
                         - rgMethyArch / YmaCH3_)
                        /86400  / FluidSystem::molarMass(RMethylIdx);

        // //check if RMethyl value not negative
        // Scalar storageRM = volVars.molarDensity(phase0Idx)
        //                  *volVars.saturation(phase0Idx)
        //                  *volVars.moleFraction(phase0Idx, RMethylIdx)
        //                  *volVars.porosity();
        // Scalar test = storageRM + q[RMethylIdx]*dt;
        // if (test < 0)
        //   q[RMethylIdx] = -storageRM/dt*0.8; // was 0.99 but might be too hard
        q[HydrogenIdx] = ((rgAmCoalBacCoal * YH2cc_ / YacbcH2_)
                          + (rgAmCoalBacAm * YH2Am_ / YacbamH2_)
                          + (rgCoalBac * YH2cc_ / YccbcH2_)
                          - rgHydroArch / YhaH2_)
                         /86400  / FluidSystem::molarMass(HydrogenIdx);
        q[TCIdx]       = (rgAcetoArch * YCO2Ac_ / YaaAc_
                         + (rgMethyArch * YCO2CH3_ / YmaCH3_)
                         -(rgHydroArch * YCO2H2_ / YhaH2_) )        // consume 1 CO2 per 1 CH4
                          /86400  / FluidSystem::molarMass(TCIdx);
        // q[TCIdx] = 0;
        q[CCoalIdx]    = -(rgCoalBac / YccbcAc_ + rgCoalBac / YccbcH2_ + rgAmCoalBacCoal / YacbcAc_ + rgAmCoalBacCoal / YacbcH2_)
                         / 86400 / SolidSystem::molarMass(CCoalPhaseIdx);


        // std::cout << "q[CH4Idx]  " << q[CH4Idx]*86400*FluidSystem::molarMass(CH4Idx)  << std::endl;
        // std::cout << "q[AcetateIdx] " << q[AcetateIdx]*86400*FluidSystem::molarMass(AcetateIdx) << std::endl;
        // std::cout << "q_reac_Am " << q[AmendmentIdx] << '\n'<<std::scientific;
        // std::cout << "q[RMethylIdx]  " << q[RMethylIdx] << std::endl;
        // std::cout << "q[HydrogenIdx]  " << q[HydrogenIdx]  << std::endl;
        // std::cout << "q[TCIdx]  " << q[TCIdx] << std::endl;
        // std::cout << "q[CCoalIdx]  " << q[CCoalIdx]  << std::endl;
        // std::cout << FluidSystem::molarMass(CCoalIdx) << " molMass CCoal" << std::endl;
  }

    void retardationSource(Sources &q,
                           const FVElementGeometry &fvGeometry,
                           const ElementVolumeVariables &elemVolVars,
                           const SubControlVolume &scv,
                           const Scalar dt,
                           const auto velocity)
    {
        const auto& volVars = elemVolVars[scv];

        Scalar cLiquidCarAm   = volVars.moleFraction(phase0Idx, AmendmentIdx)
                                * volVars.molarDensity(phase0Idx)
                                * FluidSystem::molarMass(AmendmentIdx); //[kg_suspended_Biomass/m³_waterphase]
        if(cLiquidCarAm < 0)
            cLiquidCarAm = 0;

        Scalar depositionRate = lambdaDepos_ * cLiquidCarAm * velocity;
        Scalar resuspensionRate = lambdaResusp_
                                  // * reactionPressureGradient(fvGeometry, elemVolVars, scv, phase0Idx)
                                  * volVars.solidVolumeFraction(SolidAmendmentPhaseIdx)
                                  * volVars.solidComponentDensity(SolidAmendmentPhaseIdx);


        // std::cout<<"depostRate "<<std::scientific<< depositionRate << std::endl;
        // std::cout<<"resuspRate "<<std::scientific<< resuspensionRate << std::endl;

        Scalar totalRate =  depositionRate - resuspensionRate;
        q[AmendmentIdx] -= totalRate;
        q[SolidAmendmentIdx] += totalRate;

    }

    // Scalar reactionPressureGradient(const FVElementGeometry& fvGeometry,
    //                                 const ElementVolumeVariables &elemVolVars,
    //                                 const SubControlVolume &scv,
    //                                 int phaseIdx)
    // {
    //     const auto& insideScvIdx = fvGeometry.scvf().insideScvIdx();
    //     const auto& outsideScvIdx = fvGeometry.scvf().outsideScvIdx();
    //
    //     const auto& insideScv = fvGeometry.scv(insideScvIdx);
    //     const auto& outsideScv = fvGeometry.scv(fvGeometry.flipScvf().insideScvIdx());
    //
    //     const auto& insideVolVars = elemVolVars[insideScv];
    //     const auto& outsideVolVars = elemVolVars[outsideScv];
    //
    //     // Obtain inside and outside pressures
    //     const auto pressureInside = insideVolVars.pressure(phaseIdx);
    //     const auto pressureOutside = outsideVolVars.pressure(phaseIdx);
    //
    //     Scalar pressureGradient = (pressureInside - pressureOutside);
    //
    //     return pressureGradient;
    // }

private:
    Scalar Kbac_;
    Scalar Karch_;

    Scalar muCoalBacCoal_;
    Scalar muAmCoalBacCoal_;
    Scalar muAmCoalBacAm_ ;
    Scalar muAcetoArch_ ;
    Scalar muHydroArch_;
    Scalar muMethyArch_ ;

    Scalar Kc_ ;
    Scalar KH2_ ;
    Scalar KAm_ ;
    Scalar KAc_ ;
    Scalar KCH3_;

    Scalar YccbcAc_;
    Scalar YccbcH2_;
    Scalar YacbcAc_;
    Scalar YacbcH2_;
    Scalar YacbamAc_;
    Scalar YacbamH2_;
    Scalar YacbamCH3_;
    Scalar amFac_;

    Scalar YhaH2_ ;
    Scalar YaaAc_ ;
    Scalar YmaCH3_ ;
    Scalar YH2cc_ ;
    Scalar YAccc_;
    Scalar YCH3cc_;
    Scalar YH2Am_ ;
    Scalar YAcAm_ ;
    Scalar YCH3Am_;
    Scalar YCH4Ac_ ;
    Scalar YCH4H2_ ;
    Scalar YCH4CH3_;
    Scalar YCO2Ac_ ;
    Scalar YCO2H2_;
    Scalar YCO2CH3_ ;

    Scalar FAcCoal_;
    Scalar FH2Coal_;
    Scalar FCH3Coal_;
    Scalar FAcAm_;
    Scalar FH2Am_;
    Scalar FCH3Am_;
    Scalar FCH4Ac_;
    Scalar FCH4H2_;
    Scalar FCH4CH3_;

    Scalar lambdaDepos_;
    Scalar lambdaResusp_;
    Scalar grainDiameter_;
    Scalar collectorEfficiency_;
};

} // end namespace

#endif
