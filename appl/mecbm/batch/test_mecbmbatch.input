# Parameter file for test case 2pmecbmbatch.
# Everything behind a '#' is a comment.
# Type "./test_mecbmbatch_box --help" for more information.

[TimeLoop]
TEnd = 1.728e7                     # duration of the simulation [s]
DtInitial = 10                     # initial time step size [s]
MaxTimeStepSize = 5300             # maximum time step size
OutputIntervall = 72000

[Grid]
UpperRight = 0.016125 0.016125
Cells = 1 1

[FluidSystem]
NTemperature =  200                # [-] number of tabularization entries
NPressure = 200                    # [-] number of tabularization entries
PressureLow = 1e4                  # [Pa] low end for tabularization of fluid properties
PressureHigh = 3e7                 # [Pa] high end for tabularization of fluid properties
TemperatureLow = 273.15            # [K] low end for tabularization of fluid properties
TemperatureHigh = 310.15           # [K] high end for tabularization of fluid properties

[Problem]
Name                = batchMECBMCoal # [-]  name for output files
ReservoirPressure   = 1.01325E5    # [Pa] Initial reservoir pressure
Temperature         = 298.15       # [K]  reservoir temperature

[Initial]
initxwCH4           = 0.584        # [-] initial gas saturation : was initSatGas
initxwH2            = 0            # [-] initial mole fraction Hydrogen (minimum value estimate)
initMassConcAmendment = 0	 	   # [kg/m3] set 1 for initial mass of carbon amendment: 1 ml with 1 g/L Amendment, total Volume of 10 ml
initxwAcetate       = 0		       # [-] initial mole fraction acetate (minimum value estimate) 1.3e-6 for testing directly
initxwRMethyl       = 0            # [-] initial mole fraction methyl (minimum value estimate)
initxwTC            = 0            # [-] initial mole fraction of total inorganic carbon
initPhiCCoal        = 8e-4         # [m3 biomass/m3 total] initial volume fraction of convertible coal 100kg/1m3, with rho= 1250 kg/m3, 1% bioconvertable)
initPhiCoalBac      = 1e-5         # [m3 biomass/m3 total] *? e-10...e-4 Initial volume fraction of primary bacteria
initPhiAmCoalBac    = 1e-5         # [m3 biomass/m3 total] *? e-10...e-4 Initial volume fraction of secondary bacteria
initPhiAcetoArch    = 1e-5         # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of acetoclastic archaea
initPhiHydroArch    = 1e-5         # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of hydrogenotroPhic archaea
initPhiMethyArch    = 1e-5         # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of methylotroPhic archaea

[Injection]

numInjections = 6 #
InjectionParamFile = injections/3AmInj.dat #


[BioCoefficients]
rhoBiofilm          = 10           # [kg/m3] Dry density of biofilm, value fitted by A. Ebigbo 2012 WRR
K                   = 2.0179541e-3 # [-] Decay rate for microbes
muCoalBacC          = 2.0179541E-01# [d-1] *? 0.3...0.8 Maximum specific growth rate for coal bacteria on coal]
muAmCoalBacC        = 2.0179541E-01# [d-1] *? 0.5...1.0 Maximum specific growth rate for amendment & coal bacteria on coal
muAmCoalBacAm       = 5.123E-01	   # [d-1] *? 0.5...1.0 +larger than muAmCoalBacC Maximum specific growth rate for amendment & coal bacteria on amendment
muAcetoArch         = 8            # [d-1] * Maximum specific growth rate for acetoclastic archaea
muHydroArch         = 1.66         # [d-1] * Maximum specific growth rate for hydrogenotrophic archaea
muMethyArch         = 2.9          # [d-1] * Maximum specific growth rate for methylotrophic archaea
Kc                  = 9.8084507E-01# [g/L] * Monod half saturation constant for coal
KH2                 = 1.7774596E-05# [g/L] * Monod half saturation constant for hydrogen
KAm                 = 4.0520626E-01# [g/L] *? 0.6...15 Monod half saturation constant for amendment (estimate ranging from 0.6 ... 15 g/L)
KAc                 = 2.8589134E-01# [g/L] * Monod half saturation constant for acetate
KCH3                = 2e-3         # [g/L] * Monod half saturation constant for methyl
YCoalBacC           = 1.0141768E-02# [-] *? Yield of primary bacteria biomass on coal  //TODO
YAmCoalBacC         = 1.0141768E-02# [-] * Yield of secondary bacteria on coal
YAmCoalBacAm        = 2.522924E-02 # [-] *? Yield of secondary bacteria on amendment
YHydroArchH2        = 1.217        # [-] * Yield of hydrogenotrophic archaea biomass on hydrogen
YAcetoArchAc        = 0.035        # [-] * Yield of acetoclastic Archaea biomass on acetate
YMethyArchCH3       = 0.02         # [-] * Yield of methylotrophic archaea biomass on methyl
YH2cc               = 4.182E-3     # [-] *? 0.1...0.001 Yield of hydrogen from coal
YAccc               = 0.1661       # [-] *? 0.1...0.001 Yield of acetate from coal
YH2Am               = 0.0172359    # [-] * Yield of hydrogen from amendment
YAcAm               = 0.684732     # [-] * Yield of acetate from amendment
YCH3Am              = 0.0          # [-] * Yield of methyl from amendment
YCH4Ac              = 0.259        # [-] * Yield of methane from acetate
YCH4H2              = 1.569        # [-] * Yield of methane from hydrogen
YCH4CH3             = 1.0          # [-] * Yield of methane from methyl
YCO2Ac              = 0.2058       # [-] * Yield of methane from acetate
YCO2CH3             = 7.26E-3      # [-] * Yield of methane from methyl

[SorptionCoefficients]
useAdsorption       = 1            # Using Ad-/Desorption in general (1: yes, 0: no)
useEL               = 1            # Using Extended Langmuir Ad-/Desorption formulation (1: yes, 0: no=simple Langmuir), useAdsorption must be 1!
VCH4                = 0.8          # [mol/l] or m3/kg? values from "Malgo's table [67]"
bCH4                = 2.3e-7       # [1/Pa] or maybe use 1/MPa !conversion 1/b=Pl with Pl =167.5 psia
VCO2                = 1.6          # [mol/l] or m3/kg?
bCO2                = 4.64e-7      # [1/Pa] !conversion 1/b=Pl with Pl =167.5 psia

[SpatialParams]
SolubilityLimit     = 0.295        # [-]  solubility limit of salt in brine
Porosity            = 0.97         # [-]  initial porosity coal 100kg/m3, with rho= 1250 kg/m3
Permeability        = 2.23e-14
IrreducibleLiqSat   = 1e-3         # [-]  irreducible liquid saturation
IrreducibleGasSat   = 1e-5         # [-]  irreducible gas saturation
Pentry1             = 500          # [Pa]
BCLambda1           = 2            # [-]

[Vtk]
AddVelocity         = 1            # Add extra information

[LinearSolver]
ResidualReduction = 1e-5

