# Parameter file for test case 2pmecbmbatch.
# Everything behind a '#' is a comment.
# Type "./test_mecbmbatch_box --help" for more information.

[TimeLoop]
TEnd = 1.495e7                    # duration of the simulation [s]
DtInitial = 0.1	                   # initial time step size [s]
MaxTimeStepSize = 21600            # maximum time step size

[Grid]
UpperRight = 0 0 0.13858  # actual length: 0.13858 + HEAD Space
Cells = 90

[FluidSystem]
NTemperature =  200                # [-] number of tabularization entries
NPressure = 200                    # [-] number of tabularization entries
PressureLow = 1e4                  # [Pa] low end for tabularization of fluid properties
PressureHigh = 3e7                 # [Pa] high end for tabularization of fluid properties
TemperatureLow = 273.15            # [K] low end for tabularization of fluid properties
TemperatureHigh = 310.15           # [K] high end for tabularization of fluid properties

[Brine]
Salinity = 1e-1

[Problem]
Name                = columnSALibBio # [-]  name for output files
ReservoirPressure   = 1.01325E5    # [Pa] Initial reservoir pressure
Temperature         = 298.15       # [K]  reservoir temperature
EnableGravity = true

[Initial]
AmConst = false #
AmLin = false #
AmLinInv = false #
initxwCH4           = 0.0        # [-] initial gas saturation : was initSatGas
initxwH2            = 0            # [-] initial mole fraction Hydrogen (minimum value estimate)
initMassConcAmendment = 15   # [kg/m3] set 1 for initial mass of carbon amendment: 1 ml with 15 g/L Amendment
initxwAcetate       = 0		       # [-] initial mole fraction acetate (minimum value estimate) 1.3e-6 for testing directly
initxwRMethyl       = 0            # [-] initial mole fraction methyl (minimum value estimate)
initxwTC            = 0            # [-] initial mole fraction of total inorganic carbon
initPhiCCoal        = 4E-4         # [m3 biomass/m3 total] initial volume fraction of convertible coal 100kg/1m3, with rho= 1250 kg/m3, 1% bioconvertable)
initPhiCoalBac      = 0#1e-5         # [m3 biomass/m3 total] *? e-10...e-4 Initial volume fraction of primary bacteria
initPhiAmCoalBac    = 0#1e-5         # [m3 biomass/m3 total] *? e-10...e-4 Initial volume fraction of secondary bacteria
initPhiAcetoArch    = 0#1e-6         # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of acetoclastic archaea
initPhiHydroArch    = 0#1e-6         # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of hydrogenotroPhic archaea
initPhiMethyArch    = 0#1e-6         # [m3 biomass/m3 total] *? e-10...e-2 Initial volume fraction of methylotroPhic archaea

[Injection]
numInjections = 21 #
InjectionParamFile = injections/2ColumnAmInjBio.dat #
injQ = 8.33E-11 # Experiment: 0.005 ml/min

[BioCoefficients]
rhoBiofilm          = 10           # [kg/m3] Dry density of biofilm, value fitted by A. Ebigbo 2012 WRR
Kbac                =                          1.000000000000000E-004 # [-] Decay rate for microbes
Karch               =                           1.000000000000000E-004# [-] Decay rate for microbes
muCoalBacC          =                       3.084593308768562E-001
muAmCoalBacC        =                       5.470979284287117E-002
muAmCoalBacAm       =                       7.713072679770711E-001
muAcetoArch         =                       2.794610984789061E-001 # [d-1] * Maximum specific growth rate for acetoclastic archaea
muHydroArch         =                       2.535232475275825E-001   # [d-1] * Maximum specific growth rate for hydrogenotrophic archaea
muMethyArch         =                       1.006527029130433E+000      # [d-1] * Maximum specific growth rate for methylotrophic archaea
Kc                  =                       5.206039589957183E-001 # [g/L] * Monod half saturation constant for coal
KH2                 =                       7.649955907957280E-005 # [g/L] * Monod half saturation constant for hydrogen
KAm                 =                       11.718733448585758E-001 # [g/L] 0.6...15
KAc                 =                       1.376984366209517E-003    # [g/L] * Monod half saturation constant for acetate
KCH3                = 2e-3         # [g/L] * Monod half saturation constant for methyl
FAcCoal             =                       1.233035980338247E+000
FH2Coal             =                       1.000000000000000E-001
FAcAm               =                       1.240799965055394E+000
FH2Am               =                       1.492289773703870E+000
FCH3Am              =                       1.053243566795457E+000
FCH4Ac              =                       1.131789295418301E+000
FCH4H2              =                       1.900000000000000E+000
FCH4CH3             =                       1.069389949902825E+000
YCoalBacCAc         = 1.49E-01  # [-] Yield of primary bacteria biomass on coal  //TODO
YCoalBacCH2         = 1.85E-02  # [-] Yield of primary bacteria biomass on coal  //TODO
YCoalBacCCH3        = 5.32E-02  # [-] Yield of primary bacteria biomass on coal  //TODO
YAmCoalBacAmAc      = 1.49E-01  # [-]  Yield of secondary bacteria on amendment
YAmCoalBacAmH2      = 1.85E-02  # [-]  Yield of secondary bacteria on amendment
YAmCoalBacAmCH3     = 5.32E-02  # [-]  Yield of secondary bacteria on amendment
amFac               = 1
YAcetoArchAc        = 3.57E-02  # [-] * Yield of acetoclastic Archaea biomass on acetate
YHydroArchH2        = 1.22E+00  # [-] * Yield of hydrogenotrophic archaea biomass on hydrogen
YMethyArchCH3       = 1.87E-01  # [-] * Yield of methylotrophic archaea biomass on methyl
YAccc               = 7.89E-01  # [-] 0.1...0.001 Yield of acetate from coal
YH2cc               = 1.30E-01  # [-]  0.1...0.001 Yield of hydrogen from coal
YCH3cc              = 6.61E-01  # [-] Yield of CH3 from coal
YAcAm               = 7.89E-01  # [-] * Yield of Acetate from amendment
YH2Am               = 1.30E-01  #
YCH3Am              = 6.61E-01  # [-] * Yield of methyl from amendment
YCH4Ac              = 2.59E-01  # [-] * Yield of methane from acetate
YCH4H2              = 1.57E+00  # [-] * Yield of methane from hydrogen
YCH4CH3             = 3.09E-01  # [-] * Yield of methane from methyl
YCO2Ac              = 7.11E-01  # [-] * Yield of methane from acetate
YCO2H2              = 2.35E-01  # [-] * Yield of methane from acetate
YCO2CH3             = 1.62E-01  # [-] * Yield of methane from methyl

[Retardation]
lambdaDepos         = 51394
lambdaResusp        = 4.96296E-06
lambdaDeposBac      =      751160
lambdaResuspBac     =      1.113526E-08


[SorptionCoefficients]
useAdsorption       = 1            # Using Ad-/Desorption in general (1: yes, 0: no)
useEL               = 1            # Using Extended Langmuir Ad-/Desorption formulation (1: yes, 0: no=simple Langmuir), useAdsorption must be 1!
VCH4                = 0.8          # [mol/l] or m3/kg? values from "Malgo's table [67]"
bCH4                = 2.3e-7       # [1/Pa] or maybe use 1/MPa !conversion 1/b=Pl with Pl =167.5 psia
VCO2                = 1.6          # [mol/l] or m3/kg?
bCO2                = 4.64e-7      # [1/Pa] !conversion 1/b=Pl with Pl =167.5 psia

[SpatialParams]
SolubilityLimit     = 0.295        # [-]  solubility limit of salt in brine
Porosity            = 0.48         # [-]  initial porosity coal 100kg/m3, with rho= 1250 kg/m3
Permeability        = 2.23e-10
IrreducibleLiqSat   = 1e-3         # [-]  irreducible liquid saturation
IrreducibleGasSat   = 1e-5         # [-]  irreducible gas saturation
Pentry1             = 500          # [Pa]
BCLambda1           = 2            # [-]

[Vtk]
AddVelocity         = 1            # Add extra information

[LinearSolver]
ResidualReduction = 1e-7

[Newton]
EnablePartialReassembly = true
EnableResidualCriterion = true
ResidualReduction = 1e-8
