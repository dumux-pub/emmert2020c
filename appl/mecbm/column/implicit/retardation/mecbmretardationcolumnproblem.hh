// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
#ifndef DUMUX_MECBM_RETARDATION_COLUMN_PROBLEM_HH
#define DUMUX_MECBM_RETARDATION_COLUMN_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/brinech4co2.hh>

#include <dumux/material/solidsystems/mecbmretardation.hh>

#include "mecbmretardationcolumnspatialparams.hh"
#include <dumux/material/chemistry/biogeochemistry/mecbmretardationreactions.hh>

#include <test/porousmediumflow/co2/implicit/co2tables.hh>

namespace Dumux {
/*!
 *
 * \brief Problem where CH4 should be produced by providing substrate (Amendment and Coal)
 * for bacteria, that produce intermediate products which serve as substrate for archaea in a coal-bed.
 */
template <class TypeTag>
class MECBMRetardationColumnProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct MECBMRetardationColumnTypeTag { using InheritsFrom = std::tuple<TwoPNCMin>; };
struct MECBMRetardationColumnBoxTypeTag { using InheritsFrom = std::tuple<MECBMRetardationColumnTypeTag, BoxModel>; };
struct MECBMRetardationColumnCCTpfaTypeTag { using InheritsFrom = std::tuple<MECBMRetardationColumnTypeTag, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::MECBMRetardationColumnTypeTag> { using type = Dune::FoamGrid<1,3>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::MECBMRetardationColumnTypeTag> { using type = MECBMRetardationColumnProblem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::MECBMRetardationColumnTypeTag> {
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::BrineCH4CO2<Scalar,
                                           HeterogeneousCO2Tables::CO2Tables,
                                           Components::TabulatedComponent<Components::H2O<GetPropType<TypeTag, Properties::Scalar>>>,
                                           FluidSystems::BrineCH4CO2DefaultPolicy</*constantSalinity=*/true, false>>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::MECBMRetardationColumnTypeTag> {
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = SolidSystems::MECBMRetardationSolidPhase<Scalar>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::MECBMRetardationColumnTypeTag>{
    using MT = GetPropType<TypeTag, ModelTraits>;
    static constexpr int numFluidComps = MT::numFluidComponents();
    static constexpr int numActiveSolidComps = MT::numSolidComps() - MT::numInertSolidComps();
    using type = MECBMRetardationColumnSpatialParams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>, numFluidComps, numActiveSolidComps>;
};

//Set properties here to override the default property settings
//template<class TypeTag>
//struct ReplaceCompEqIdx<TypeTag, TTag::MECBMRetardationColumnTypeTag> { static constexpr int value = 16; }; //!< Replace gas balance by total mass balance

// Default formulation is pw-Sn, overwrite if necessary
template<class TypeTag>
struct Formulation<TypeTag, TTag::MECBMRetardationColumnTypeTag>
{ static constexpr auto value = TwoPFormulation::p0s1; };

// Enable caching or not (reference solutions created without caching)
// SET_BOOL_PROP(MECBMRetardationColumnTypeTag, EnableGridGeometryCache, true);
// SET_BOOL_PROP(MECBMRetardationColumnTypeTag, EnableGridVolumeVariablesCache, true);
// SET_BOOL_PROP(MECBMRetardationColumnTypeTag, EnableGridFluxVariablesCache, true);

}

/*!
 * \ingroup TwoPNCMinModel
 * \ingroup ImplicitTestProblems
 * \brief Problem where water is injected to flush precipitated salt in a gas reservoir clogged due to precipitated salt.
 *
 * The domain is sized 10m times 20m and contains a vertical low-permeable layer of precipitated salt near an extraction well.
 *
 * To flush this precipitated salt, water is injected through the gas extraction well in order to dissolve the precipitated salt increasing the permeability and thereby achieving high gas extraction rates later. Here, the system is assumed to be isothermal.
 * Neumann no-flow boundary condition is applied at the top and bottom boundary and Dirichlet boundary condition is used on the right and left sides.
 * The injected water phase migrates downwards due to increase in density as the precipitated salt dissolves.
 *
 * The model uses mole fractions of dissolved components and volume fractions of precipitated salt as primary variables. Make sure that the according units are used in the problem setup.
 *
 * This problem uses the \ref TwoPNCMinModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2pncmin</tt>
 */
template <class TypeTag>
class MECBMRetardationColumnProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using SolidSystem = GetPropType<TypeTag, Properties::SolidSystem>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using Chemistry = typename Dumux::MECBMRetardationReactionsChemistry<TypeTag>;
    using Indices = TwoPNCIndices;

    enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx   = Indices::switchIdx, //Saturation

        //Indices of the phases
        phase0Idx = FluidSystem::phase0Idx,
        phase1Idx = FluidSystem::phase1Idx,

        //Indices of the components
        numComponents = FluidSystem::numComponents,
        comp0Idx    = FluidSystem::BrineIdx,
        comp1Idx    = FluidSystem::CH4Idx,
        AcetateIdx  = FluidSystem::AcetateIdx,
        AmendmentIdx= FluidSystem::AmendmentIdx,
        RMethylIdx  = FluidSystem::RMethylIdx,
        H2Idx       = FluidSystem::H2Idx,
        TCIdx       = FluidSystem::TCIdx,

        //Indices of the bio/coal volume fractions
        CoalBacIdx   = SolidSystem::CoalBacPhaseIdx + numComponents,
        AmCoalBacIdx = SolidSystem::AmCoalBacPhaseIdx + numComponents,
        AcetoArchIdx = SolidSystem::AcetoArchPhaseIdx + numComponents,
        HydroArchIdx = SolidSystem::HydroArchPhaseIdx + numComponents,
        MethyArchIdx = SolidSystem::MethyArchPhaseIdx + numComponents,
        CCoalIdx     = SolidSystem::CCoalPhaseIdx + numComponents,
        SolidAmendmentIdx  = SolidSystem::SolidAmendmentPhaseIdx + numComponents,


        //Index of the primary component of G and L phase
        conti0EqIdx = Indices::conti0EqIdx,

        // Phase State
        wPhaseOnly = Indices::firstPhaseOnly,
        nPhaseOnly = Indices::secondPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim      = GridView::dimension,
        dimWorld = GridView::dimensionworld,
    };

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;
    using CoordScalar = typename GridView::ctype;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using FluxVariables = GetPropType<TypeTag, Properties::FluxVariables>;
    using VelocityBackend = PorousMediumFlowVelocity<GridVariables, FluxVariables>;
    using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    //! property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
    static constexpr auto numPhases = FluidSystem::numPhases;

public:
    MECBMRetardationColumnProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        name_                   = getParam<std::string>("Problem.Name");
        temperature_            = getParam<Scalar>("Problem.Temperature");
        reservoirPressure_      = getParam<Scalar>("Problem.ReservoirPressure");

        initxwCH4_              = getParam<Scalar>("Initial.initxwCH4"); // Initial wetting mole fraction of CH4
        initxwH2_               = getParam<Scalar>("Initial.initxwH2"); // Initial wetting mole fraction of Hydrogen
        initMassConcAmendment   = getParam<Scalar>("Initial.initMassConcAmendment"); // Initial mass concentration of Amendment
        initxwAcetate_          = getParam<Scalar>("Initial.initxwAcetate"); // Initial wetting mole fraction of Acetate
        initxwRMethyl_          = getParam<Scalar>("Initial.initxwRMethyl"); // Initial wetting mole fraction of Methyl
        initxwTC_               = getParam<Scalar>("Initial.initxwTC"); // Initial wetting mole fraction of total inorganic carbon
        initPhiCCoal_           = getParam<Scalar>("Initial.initPhiCCoal"); // Initial volume fraction of bioavailable coal
        initPhiCoalBac_         = getParam<Scalar>("Initial.initPhiCoalBac"); // Initial volume fraction of coal consuming bacteria
        initPhiAmCoalBac_       = getParam<Scalar>("Initial.initPhiAmCoalBac"); // Initial volume fraction of amendment and coal consuming bacteria
        initPhiAcetoArch_       = getParam<Scalar>("Initial.initPhiAcetoArch"); // Initial volume fraction of acetoclastics archaea
        initPhiHydroArch_       = getParam<Scalar>("Initial.initPhiMethyArch"); // Initial volume fraction of methylotrophic archaea
        initPhiMethyArch_       = getParam<Scalar>("Initial.initPhiHydroArch"); // Initial volume fraction of hydrogenotrophic archaea

        numInjections_          = getParam<int>("Injection.numInjections");
        injectionParameters_    = getParam<std::string>("Injection.InjectionParamFile");
        injQ_                   = getParam<Scalar>("Injection.injQ");

        nTemperature_           = getParam<int>("FluidSystem.NTemperature");
        nPressure_              = getParam<int>("FluidSystem.NPressure");
        pressureLow_            = getParam<Scalar>("FluidSystem.PressureLow");
        pressureHigh_           = getParam<Scalar>("FluidSystem.PressureHigh");
        temperatureLow_         = getParam<Scalar>("FluidSystem.TemperatureLow");
        temperatureHigh_        = getParam<Scalar>("FluidSystem.TemperatureHigh");

        std::ifstream injectionData;
        std::string row;
        injectionData.open( injectionParameters_); // open the Injection data file
        if (not injectionData.is_open())
        {
            std::cerr << "\n\t -> Could not open file '"
                      << injectionParameters_
                      << "'. <- \n\n\n\n";
            exit(1) ;
        }
        int tempType = 0;

        // print file to make sure it is the right file
        std::cout << "Read file: " << injectionParameters_ << " ..." << std::endl;
        while(!injectionData.eof())
        {
            getline(injectionData, row);
            std::cout << row << std::endl;
        }
        injectionData.close();

        // read data from file
        injectionData.open(injectionParameters_);

        while(!injectionData.eof())
        {
            getline(injectionData, row);

            if(row == "InjectionTypes")
            {
                getline(injectionData, row);
                while(row != "#")
                {
                    if (row != "#")
                    {
                        std::istringstream ist(row);
                        ist >> tempType;
                        injType_.push_back(tempType);
                        std::cout << "size of injType: "<<injType_.size() << std::endl;
                    }
                    getline(injectionData, row);
                }
            }
        }

        injectionData.close();

        if (injType_.size() != numInjections_)
        {
            std::cerr <<  "numInjections from the parameterfile and the number of injection types specified in the injection data file do not match!"
                      <<"\n numInjections from parameter file = "<<numInjections_
                      <<"\n numInjTypes from injection data file = "<<injType_.size()
                      <<"\n Abort!\n";
            exit(1) ;
        }

        unsigned int codim = GetPropType<TypeTag, Properties::GridGeometry>::discMethod == DiscretizationMethod::box ? dim : 0;
        Kxx_.resize(gridGeometry->gridView().size(codim));
        Kyy_.resize(gridGeometry->gridView().size(codim));
        Kzz_.resize(gridGeometry->gridView().size(codim));
        cellVolume_.resize(gridGeometry->gridView().size(codim));
        molOutFluxCH4w_.resize(gridGeometry->gridView().size(codim));
        molOutFluxCH4n_.resize(gridGeometry->gridView().size(codim));
        molOutFlux_.resize(numPhases);

        soluteAmendment_.resize(gridGeometry->gridView().size(codim));
        concSolidAmendment_.resize(gridGeometry->gridView().size(codim));
        concCCoal_.resize(gridGeometry->gridView().size(codim));

        FluidSystem::init(/*Tmin=*/temperatureLow_,
                          /*Tmax=*/temperatureHigh_,
                          /*nT=*/nTemperature_,
                          /*pmin=*/pressureLow_,
                          /*pmax=*/pressureHigh_,
                          /*np=*/nPressure_);
    }

    void setGridVariables(std::shared_ptr<GridVariables> gridVariables)
    { gridVariables_ = gridVariables; }

    const GridVariables& gridVariables() const
    { return *gridVariables_; }

    void generateVelocityBackend()
    { velocityBackend_ = std::make_unique<VelocityBackend>(*gridVariables_); }

    void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }

    void setEpisodeIdx( Scalar epiIdx )
    {
        episodeIdx_ = epiIdx;
    }

    int injectionType( Scalar epiIdx )
    {
        return injType_[epiIdx];
    }


    /*!
     * \name Problem parameters
     */


    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return temperature_; }

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        // default to Neumann
        bcTypes.setAllNeumann();

       // const Scalar zMax = this->gridGeometry().bBoxMax()[dim-1];
       // if (time_ > 1.72e7 && globalPos[1] > zMax - eps_ )
       //     bcTypes.setDirichlet(pressureIdx);

        return bcTypes;
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * This is the method for the case where the Neumann condition is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The sub control volume face
     *
     * For this method, the \a values parameter stores the flux
     * in normal direction of each phase. Negative values mean influx.
     * E.g. for the mass balance that would the mass flux in \f$ [ kg / (m^2 \cdot s)] \f$.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector flux(0.0);

        const auto& ipGlobal = scvf.ipGlobal();
        const auto& volVars = elemVolVars[scvf.insideScvIdx()];


        Scalar area = M_PI*0.0525*0.0525/4;

        Scalar waterFlux = injQ_/area;//*0.49/area;// [m/s]

        //! Inflow boundary at bottom

        int injProcess = injType_[episodeIdx_];

        if(ipGlobal[dimWorld-1] < eps_) // standard: no injection
        {
            if (injProcess == -99) // no injection
            {
                flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx) / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux-99 \n"<<flux <<std::endl;

            }
            else if (injProcess == 1) //water injection
            {
                flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx)  / FluidSystem::molarMass(comp0Idx);
//                std::cout<<"InFlux1 \n"<<flux <<std::endl;

//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
            }
            else if (injProcess == 2) //water injection with Amendment
            {
                flux[AmendmentIdx] = - 1e-6 *initMassConcAmendment // 1 ml * 15 mg/ ml --> 15 kg
                                     / FluidSystem::molarMass(AmendmentIdx) // 0.113 kg/mol -> mol
                                     / area // m2 -> mol/m2
                                     / 240; // s-> mol/(m2 s)  (240 s injection)
                flux[comp0Idx] = - waterFlux * volVars.density(phase0Idx)  / FluidSystem::molarMass(comp0Idx)
                                 + flux[AmendmentIdx]*-1;
            }
            else if (injProcess == 0) // water injection
            {
                flux = 0.0;
//                flux[comp0Idx] = 0;
//                flux[comp1Idx] = - waterFlux * 997 / FluidSystem::molarMass(comp0Idx);
            }
        }
        else
            flux = 0.0; // kg/m/s

        // no-flow everywhere except at the top boundary
       if(ipGlobal[dimWorld-1] < this->gridGeometry().bBoxMax()[dimWorld-1] - eps_)
           return flux;

        //! Outflow boundary at top

        // set a fixed pressure on the top of the domain
        const Scalar dirichletPressure = reservoirPressure_;

        // evaluate the gradient
        const auto gradient = [&](int phaseIdx)->GlobalPosition
        {
            const auto& scvCenter = fvGeometry.scv(scvf.insideScvIdx()).center();
            const Scalar scvCenterPressureSol = volVars.pressure(phaseIdx);
//            auto grad = ipGlobal - scvCenter;
            auto grad = scvf.unitOuterNormal();
//            grad /= grad.two_norm2(); // ?
            grad *= -(dirichletPressure - scvCenterPressureSol)/ (ipGlobal -scvCenter).two_norm();
            return grad;
        };

        const Tensor K = volVars.permeability();

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx )
        {
            Scalar tpfaFlux = 0;
            const Scalar density = useMoles ? volVars.molarDensity(phaseIdx) : volVars.density(phaseIdx);

            // calculate the flux
            tpfaFlux = vtmv(gradient(phaseIdx), K, scvf.unitOuterNormal());
            auto gravity = this->spatialParams().gravity(fvGeometry.scv(scvf.insideScvIdx()).dofPosition());
            gravity *= density;
            tpfaFlux -= vtmv(gravity, K, scvf.unitOuterNormal());
            // std::cout << "tpfaFlux before density*mobility*area" << tpfaFlux<< " of compIdx "<< compIdx<< std::endl;
            tpfaFlux *=  density * volVars.mobility(phaseIdx) * scvf.area();
            // std::cout << "density " << density << " mobility "<< volVars.mobility(phaseIdx)<< std::endl;
            // std::cout << "tpfaFlux before safety " << tpfaFlux<< " of compIdx "<< compIdx<< std::endl;

            //for safety: enforce outflow only in case some newton iteration goes wrong
            if (tpfaFlux < 0.0 - eps_)
            {
                // std::cout<<"negative Outflow at top"<<std::endl;
                // tpfaFlux = 0.0;
            }

            for (int compIdx = 0; compIdx < numComponents; ++compIdx )
            {

                // std::cout << "tpfaFlux " << tpfaFlux<< " of compIdx "<< compIdx<< std::endl;
                // emulate an outflow condition for the component transport on the top side
                tpfaFlux  *= (useMoles ? volVars.moleFraction(phaseIdx, compIdx) : volVars.massFraction(phaseIdx, compIdx));
                flux[compIdx] += tpfaFlux;
                // std::cout << "flux[compIdx] " << flux[compIdx]<< " of compIdx "<< compIdx<< std::endl;
                // if (compIdx == comp1Idx)
                //     {
                //         if (phaseIdx == phase0Idx)
                //             CH4mobFluxw_ = volVars.mobility(phaseIdx)*volVars.moleFraction(phaseIdx, compIdx)*volVars.molarDensity(phaseIdx);
                //         else
                //             CH4mobFluxn_ = volVars.mobility(phaseIdx)*volVars.moleFraction(phaseIdx, compIdx)*volVars.molarDensity(phaseIdx);
                //     }


            }

            if (tpfaFlux >= 0.0 - eps_ && volVars.saturation(phaseIdx) <= 1.0 + eps_)
            {
                molOutFlux_[phaseIdx] = volVars.mobility(phaseIdx)*volVars.moleFraction(phaseIdx,comp1Idx);
                // std::cout<<"TopFlux \n"<<tpfaFlux*scvf.area() <<std::endl;
            }
            else if (tpfaFlux < 0.0 - eps_)
            {
                molOutFlux_[phaseIdx] = 0.0;
                // std::cout<<"Sat <0 \n"<<tpfaFlux*scvf.area() <<std::endl;
            }
            else if (volVars.saturation(phaseIdx) > 1.0 + eps_)
            {
                molOutFlux_[phaseIdx] = 0.0;
                // std::cout<<"Sat >1 \n"<<tpfaFlux*scvf.area() <<std::endl;
            }
            else
                std::cout << "Unkown outflow phaseIdx : " << phaseIdx << "\n value"<< tpfaFlux*scvf.area()<<  std::endl;
        }
    //        std::cout<<"TopFlux \n"<<flux <<std::endl;

        return flux;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars[pressureIdx] = reservoirPressure_;
        return priVars;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables priVars(0.0);

        auto bBoxMax = this->gridGeometry().bBoxMax()[dimWorld-1];

        priVars.setState(wPhaseOnly);
        priVars[pressureIdx]  = reservoirPressure_;
        priVars[switchIdx]    = initxwCH4_;                 // Sw primary variable

        // components
        priVars[H2Idx]        = initxwH2_;                  // Initial wetting mole fraction of Hydrogen

        if (globalPos[dimWorld-1] < bBoxMax*0.95)
        {
            priVars[AcetateIdx]   = initxwAcetate_;             // Initial wetting mole fraction of Acetate
            priVars[RMethylIdx]   = initxwRMethyl_;             // Initial wetting mole fraction of Methyl
            priVars[TCIdx]        = initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon
            // biofilm/coal volume fractions
            priVars[CCoalIdx]     = initPhiCCoal_;              // Initial volume fraction of bioavailable coal
            priVars[CoalBacIdx]   = initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
            priVars[AmCoalBacIdx] = initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
            priVars[AcetoArchIdx] = initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
            priVars[MethyArchIdx] = initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
            priVars[HydroArchIdx] = initPhiHydroArch_ ;         // Initial volume fraction of methylotrophic archaea
            priVars[SolidAmendmentIdx] = 0.0 ;         // Initial volume fraction of attached amendment

            Scalar Amendment = 1e-6 * initMassConcAmendment / FluidSystem::molarMass(AmendmentIdx);
            bool AmConst = getParam<bool>("Initial.AmConst");
            bool AmLin = getParam<bool>("Initial.AmLin");
            bool AmLinInv = getParam<bool>("Initial.AmLinInv");

            if(AmConst)
            {
                Scalar massAmConst = Amendment;
                priVars[AmendmentIdx] = massAmConst;
                std::cout << "Constant inital amendment" << std::endl;

            }
            else if (AmLin)
            {
                Scalar massAmLin = 2*Amendment * (1 - globalPos[dimWorld-1]/(bBoxMax*0.8));
                priVars[AmendmentIdx] = massAmLin;
                std::cout << "Linear inital amendment" << std::endl;
            }
            else if (AmLinInv)
            {
                Scalar massAmLinInv = 2*Amendment * (globalPos[dimWorld-1]/(bBoxMax*0.8));
                priVars[AmendmentIdx] = massAmLinInv;
                std::cout << "Linear inverse inital amendment" << std::endl;

            }
        }
        else
        {
            priVars[AmendmentIdx] = 0;
            std::cout << "No inital amendment" << std::endl;
        }

        return priVars;
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * This is the method for the case where the source term is
     * potentially solution dependent and requires some quantities that
     * are specific to the fully-implicit method.
     *
     * \param values The source and sink values for the conservation equations in units of
     *                 \f$ [ \textnormal{unit of conserved quantity} / (m^3 \cdot s )] \f$
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param elemVolVars All volume variables for the element
     * \param scv The subcontrolvolume
     *
     * For this method, the \a values parameter stores the conserved quantity rate
     * generated or annihilate per volume unit. Positive values mean
     * that the conserved quantity is created, negative ones mean that it vanishes.
     * E.g. for the mass balance that would be a mass rate in \f$ [ kg / (m^3 \cdot s)] \f$.
     */
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        Chemistry chemistry;
        const auto& volVars = elemVolVars[scv];

        const auto dofIdxGlobal = scv.dofIndex();
        Scalar vel = retVelocity(phase0Idx,dofIdxGlobal);

        chemistry.reactionSource(source, volVars, timeStepSize_);
        chemistry.retardationSource(source, fvGeometry, elemVolVars, scv, timeStepSize_, vel);

        return source;
    }

    void calcVelocity(const SolutionVector& curSol)
    {
        // using Scalar = typename SolutionVector::field_type;

        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            if(dim == 1) //isBox &&
                velocity_[phaseIdx].resize(this->gridGeometry().gridView().size(0));
            else
                velocity_[phaseIdx].resize(this->gridGeometry().numDofs());
        }
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            // const auto eIdxGlobal = this->gridGeometry().elementMapper().index(element);

            auto fvGeometry = localView(this->gridGeometry());
            auto elemVolVars = localView(gridVariables().curGridVolVars());
            auto elemFluxVarsCache = localView(gridVariables_->gridFluxVarsCache());

            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, curSol);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
            {
                velocityBackend_->calculateVelocity(velocity_[phaseIdx], element, fvGeometry, elemVolVars, elemFluxVarsCache, phaseIdx);

                // for (auto&& scv : scvs(fvGeometry))
                // {
                //     const auto dofIdxGlobal = scv.dofIndex();
                //     if (isBox && dim == 1)
                //         velocity_[phaseIdx][dofIdxGlobal] = velocity[phaseIdx][eIdxGlobal].two_norm();
                //     else
                //         velocity_[phaseIdx][dofIdxGlobal] = velocity[phaseIdx][dofIdxGlobal].two_norm();
                // }//end scvs
            } //end phases
        } //end elements
    } // end calcVelocity

    const Scalar retVelocity(const unsigned int phaseIdx,
                             const unsigned int dofIdxGlobal) const
    {
        return std::max(0.0,velocity_[phaseIdx][dofIdxGlobal].two_norm());
    }

    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */

    const std::vector<Scalar>& getKxx()
    {
        return Kxx_;
    }

    const std::vector<Scalar>& getKyy()
    {
        return Kyy_;
    }

    const std::vector<Scalar>& getKzz()
    {
        return Kzz_;
    }

    const std::vector<Scalar>& cellVolume()
    {
        return cellVolume_;
    }

    const auto& molOutFluxCH4w()
    {
        return molOutFluxCH4w_;
    }

    const auto& molOutFluxCH4n()
    {
        return molOutFluxCH4n_;
    }

    const auto& soluteAmendment()
    {
        return soluteAmendment_;
    }

    /*!
     * \brief Return how much the domain is extruded at a given sub-control volume.
     *
     * This means the factor by which a lower-dimensional (1D or 2D)
     * entity needs to be expanded to get a full dimensional cell. The
     * default is 1.0 which means that 1D problems are actually
     * thought as pipes with a cross section of 1 m^2 and 2D problems
     * are assumed to extend 1 m to the back.
     */
    template<class ElementSolution>
    Scalar extrusionFactor(const Element &element,
                           const SubControlVolume &scv,
                           const ElementSolution& elemSol) const
    {
        const auto radius = 0.0525/2;
        return M_PI*radius*radius;
    }

    void updateVtkOutput(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto elemSol = elementSolution(element, curSol, this->gridGeometry());
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();
                Kxx_[dofIdxGlobal] = volVars.permeability()[0][0];
                Kyy_[dofIdxGlobal] = volVars.permeability()[1][1];
                Kzz_[dofIdxGlobal] = volVars.permeability()[2][2];
                cellVolume_[dofIdxGlobal] = element.geometry().volume();
                molOutFluxCH4w_[dofIdxGlobal] = molOutFlux_[phase0Idx];
                molOutFluxCH4n_[dofIdxGlobal] = molOutFlux_[phase1Idx];
                soluteAmendment_[dofIdxGlobal] = volVars.molarDensity(phase0Idx)
                                                 *volVars.moleFraction(phase0Idx, AmendmentIdx)
                                                 *volVars.porosity()
                                                 *volVars.saturation(phase0Idx);
                concSolidAmendment_[dofIdxGlobal] = volVars.solidVolumeFraction(SolidSystem::SolidAmendmentPhaseIdx)*volVars.solidComponentDensity(SolidSystem::SolidAmendmentPhaseIdx);
                concCCoal_[dofIdxGlobal] = volVars.solidVolumeFraction(SolidSystem::CCoalPhaseIdx)*volVars.solidComponentDensity(SolidSystem::CCoalPhaseIdx);
            }
        }

        writeFluxes();
        writeEpisodeFluxes(0);
    }

    void writeFluxes()
    {
        // filename of the output file
        std::string fileName = this->name();
        fileName += ".dat";
        std::ofstream dataFile;
        if (time_ == 0.0)
        {
            dataFile.open(fileName.c_str());
            dataFile << "#time timeStepSize molCH4w molCH4n sumSoluteAmendment sumSolidAmendment sumCCoal \n";
            dataFile.close();
        }

        const Scalar time = time_;
        const Scalar timeStepSize = timeStepSize_;
        const Scalar molOutFluxCH4w = molOutFlux_[phase0Idx];
        const Scalar molOutFluxCH4n = molOutFlux_[phase1Idx];
        const Scalar sumSoluteAmendment = std::accumulate(soluteAmendment_.begin(),soluteAmendment_.end(),0.0);
        const Scalar sumSolidAmendment = std::accumulate(concSolidAmendment_.begin(),concSolidAmendment_.end(),0.0);
        const Scalar sumCCoal = std::accumulate(concCCoal_.begin(),concCCoal_.end(),0.0);
        dataFile.open(fileName, std::ios::app);
        //time ; Sw ; Sn ; poro ; mwCh4 ; mnCh4
        dataFile<< time
                <<" "
                << timeStepSize
                << " "
                << molOutFluxCH4w
                << " "
                << molOutFluxCH4n
                << " "
                << sumSoluteAmendment
                << " "
                << sumSolidAmendment
                << " "
                << sumCCoal
                ;
        dataFile << "\n";
        dataFile.close();
    }

    void writeEpisodeFluxes(int episodeIdx)
    {
        // filename of the output file
        std::string fileName = this->name();
        fileName += "Episodes.out";
        std::ofstream dataFile;

        if (time_ == 0.0)
        {
            dataFile.open(fileName.c_str());
            // dataFile << "0.0"<< "\n";
            dataFile.close();
        }

        if (episodeIdx == 0)
        {
            molOutFluxCH4Sum_ += molOutFlux_[phase0Idx]*timeStepSize_/1e8;
            molOutFluxCH4Sum_ += molOutFlux_[phase1Idx]*timeStepSize_/1e8;
        }
        else if (episodeIdx == 1)
        {
            molOutFluxCH4Sum_ += molOutFlux_[phase0Idx]*timeStepSize_/1e8;
            molOutFluxCH4Sum_ += molOutFlux_[phase1Idx]*timeStepSize_/1e8;

            dataFile.open(fileName, std::ios::app);
            dataFile<< molOutFluxCH4Sum_;
            dataFile << "\n";
            dataFile.close();
        }
        else if (episodeIdx == 2)
        {
            molOutFluxCH4Sum_ += molOutFlux_[phase0Idx]*timeStepSize_/1e8;
            molOutFluxCH4Sum_ += molOutFlux_[phase1Idx]*timeStepSize_/1e8;

        }

    }

private:

    /*!
     * \brief Returns the molality of NaCl (mol NaCl / kg water) for a given mole fraction
     *
     * \param XwNaCl the XwNaCl [kg NaCl / kg solution]
     */
    static Scalar massToMoleFrac_(Scalar XwNaCl)
    {
       const Scalar Mw = 18.015e-3; //FluidSystem::molarMass(comp0Idx); /* molecular weight of water [kg/mol] */ //TODO use correct link to FluidSyswem later
       const Scalar Ms = 58.44e-3;  //FluidSystem::molarMass(NaClIdx); /* molecular weight of NaCl  [kg/mol] */

       const Scalar X_NaCl = XwNaCl;
       /* XwNaCl: conversion from mass fraction to mol fraction */
       auto xwNaCl = -Mw * X_NaCl / ((Ms - Mw) * X_NaCl - Ms);
       return xwNaCl;
    }

    int nTemperature_;
    int nPressure_;
    std::string name_;

    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    Scalar reservoirPressure_;
    Scalar temperature_;

    Scalar initxwCH4_;                 // Initial wetting mole fraction of methane
    Scalar initxwH2_;                  // Initial wetting mole fraction of Hydrogen
    Scalar initMassConcAmendment;      // Initial wetting mole fraction of Amendment
    Scalar initxwAcetate_;             // Initial wetting mole fraction of Acetate
    Scalar initxwRMethyl_;             // Initial wetting mole fraction of Methyl
    Scalar initxwTC_;                  // Initial wetting mole fraction of total inorganic carbon

    // biofilm/coal volume fractions
    Scalar initPhiCCoal_;              // Initial volume fraction of bioavailable coal
    Scalar initPhiCoalBac_;            // Initial volume fraction of coal consuming bacteria
    Scalar initPhiAmCoalBac_;          // Initial volume fraction of amendment and coal consuming bacteria
    Scalar initPhiAcetoArch_;          // Initial volume fraction of acetoclastics archaea
    Scalar initPhiMethyArch_;          // Initial volume fraction of hydrogenotrophic archaea
    Scalar initPhiHydroArch_;          // Initial volume fraction of methylotrophic archaea

    std::vector<int> injType_;
    int numInjections_;                // number of Injections
    std::string injectionParameters_;  // injectionParamteres from file
    Scalar injQ_;                      // injected volume flux in m3/s

    Scalar time_ = 0.0;
    Scalar timeStepSize_ = 0.0;
    Scalar episodeIdx_ = 0;

    static constexpr Scalar eps_ = 1e-6;
    std::vector<double> Kxx_;
    std::vector<double> Kyy_;
    std::vector<double> Kzz_;
    std::vector<double> cellVolume_;
    std::vector<double> molOutFluxCH4w_;
    std::vector<double> molOutFluxCH4n_;
    Scalar molOutFluxCH4Sum_ = 0.0;
    std::vector<double> soluteAmendment_;
    std::vector<double> concSolidAmendment_;
    std::vector<double> concCCoal_;

    std::shared_ptr<GridVariables> gridVariables_;

    std::unique_ptr<VelocityBackend> velocityBackend_;
    std::array<std::vector<Dune::FieldVector<Scalar, 3> > , numPhases> velocity_;

    mutable std::vector<double> molOutFlux_;
};

} //end namespace Dumux

#endif
