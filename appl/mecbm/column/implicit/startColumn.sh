#!/bin/sh
#ColumnCoal00-0.01

echo "Starting all standard column simulations"
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal00.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal01.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal10.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal11.input -TimeLoop.TEnd 3e7

./test_mecbmcolumn_tpfa test_mecbmcolumnCoal10Clogging.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal11Clogging.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal01Clogging.input -TimeLoop.TEnd 3e7

./test_mecbmcolumn_tpfa test_mecbmcolumnCoal10CloggingLessBiofilm.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal11CloggingLessBiofilm.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal01CloggingLessBiofilm.input -TimeLoop.TEnd 3e7

./test_mecbmcolumn_tpfa test_mecbmcolumnCoal00LessBiofilm.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal01LessBiofilm.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal10LessBiofilm.input -TimeLoop.TEnd 3e7
./test_mecbmcolumn_tpfa test_mecbmcolumnCoal11LessBiofilm.input -TimeLoop.TEnd 3e7

echo "Done with all standard column simulations"
