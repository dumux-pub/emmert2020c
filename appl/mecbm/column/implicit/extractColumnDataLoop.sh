#!/bin/sh

#Standard Loop
for j in 00 00LessBiofilm 01 01Clogging 01CloggingLessBiofilm 01LessBiofilm 10 10Clogging 10CloggingLessBiofilm 10LessBiofilm 11 11Clogging 11CloggingLessBiofilm 11LessBiofilm
    do
        for i in 01 043 086 13  # First loop with amendment injections (0 for no, 1 for injection).
            do
                pvpython ../../../../../../dumux/bin/postprocessing/extractpointdataovertime.py -f columnMECBMCoal$j.pvd -o . -of columnMECBMCoal$j-$i -p 0.0 0.0 0.$i
                echo "run pvpython extractpointdataovertime.py of columnMECBMCoal$j at point $i done"
            done
    done

echo "Standard Loop Done"
