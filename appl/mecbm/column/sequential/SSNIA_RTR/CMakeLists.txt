dune_symlink_to_source_files(FILES input)
dune_symlink_to_source_files(FILES run.log)

# isothermal tests
dune_add_test(NAME SSNIA_RTRimplicit
            SOURCES test_mecbmcolumn_fv.cc)

dune_symlink_to_source_files(FILES injections)
