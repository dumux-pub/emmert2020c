dune_symlink_to_source_files(FILES test_mecbmcolumnCoal00.input)
dune_symlink_to_source_files(FILES test_mecbmcolumnCoal01.input)
dune_symlink_to_source_files(FILES test_mecbmcolumnCoal10.input)
dune_symlink_to_source_files(FILES test_mecbmcolumnCoal11.input)
dune_symlink_to_source_files(FILES onlycoalbac.input)
dune_symlink_to_source_files(FILES input)
dune_symlink_to_source_files(FILES run.log)

# isothermal tests
dune_add_test(NAME SNI_T+R_implicit
            SOURCES test_mecbmcolumn_fv.cc)

dune_symlink_to_source_files(FILES injections)
