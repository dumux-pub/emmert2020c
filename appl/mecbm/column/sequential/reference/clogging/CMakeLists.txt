dune_symlink_to_source_files(FILES test_mecbmcolumnCoal10CloggingLessBiofilm.input)
dune_symlink_to_source_files(FILES test_mecbmcolumnCoal11CloggingLessBiofilm.input)
dune_symlink_to_source_files(FILES bioparams.input)

# isothermal tests
dune_add_test(NAME test_clog_reference_tpfa
            SOURCES test_clog_reference_tpfa.cc
            COMPILE_DEFINITIONS TYPETAG=MECBMColumnCCTpfaTypeTag)

dune_symlink_to_source_files(FILES injections)
