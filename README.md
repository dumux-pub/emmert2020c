# Emmert2020c

Welcome to the dumux pub module for Emmert2020c
======================================================

This module contains the source code for the examples in the
in the PhD Thesis of

__S. Emmert (2021)__, _[Developing and Calibrating a Numerical Model for Microbially Enhanced Coal-Bed Methane Production
](http://dx.doi.org/10.18419/opus-11631)_

```
@phdthesis{emmert2021developing,
  title = {Developing and calibrating a numerical model for microbially enhanced coal-bed methane production},
  author = {Emmert, Simon},
  year = {2021},
  doi = {10.18419/opus-11631},
  publisher = {Stuttgart: Eigenverlag des Instituts f{\"u}r Wasser- und Umweltsystemmodellierung der Universit{\"a}t Stuttgart},
  school = {Universit{\"a}t Stuttgart},
  series = {Mitteilungen / Institut für Wasser- und Umweltsystemmodellierung, Universit{\"a}t Stuttgart},
  type = {Dissertation},
}
```

For building from source create a folder and download [installemmert2020c.sh](https://git.iws.uni-stuttgart.de/dumux-pub/emmert2020c/-/raw/master/installemmert2020c.sh?inline=false) from this repository.
```bash
chmod +x installemmert2020c.sh
./installemmert2020c.sh
```
will download configure and compile all dune dependencies. Furthermore you need to have the following basic requirement installed

* C, C++ compiler (C++14 required)
* Fortran compiler (gfortran)
* UMFPack from SuiteSparse



## Dependencies on other DUNE modules

| module | branch | commit hash |
|:-------|:-------|:------------|
| dune-common | releases/2.7 | 20a8172d68a26d3f87457c3e2f2a7a7e24f609db |
| dune-geometry | releases/2.7 | fb75493b000410cbd157927f80c68ec9e86c5553 |
| dune-grid | releases/2.7 | 550c58dc9c75ebf9ff2d640bd5ccbfd100714ffe |
| dune-localfunctions | releases/2.7 | ad588f9599b12b1615e760ae0106594f2059ab3e |
| dune-istl | releases/2.7 | 399ed5996e0b15c2038290810ad0f07d5f1e4c30 |
| dune-foamgrid | releases/2.7 | d49187be4940227c945ced02f8457ccc9d47536a |
| dumux | master | 398f8fb841e1ace6132f265b482df1147ad46b1b |

__Batch__
The batch examples are in the folder
```
emmert2020c/build-cmake/appl/mecbm/batch

```

All batch examples are compiled by the following commands

```bash
cd emmert2020c/build-cmake/
make test_mecbmbatch_tpfa
```

Run a batch example via (e.g. Coal+++ case is identified with Coal111)
```bash
cd emmert2020c/build-cmake/appl/mecbm/batch
./test_mecbmbatch_tpfa test_mecbmbatchCoal111.input
```

__Fully implicit (GIA) column study and Sensitivity__

The fully implicit (GIA) column examples are in the folder
```
emmert2020c/build-cmake/appl/mecbm/column/implicit/
emmert2020c/build-cmake/appl/mecbm/column/implicit/retardation/
emmert2020c/build-cmake/appl/mecbm/column/implicit/bioretardation/
```

The fully implicit (GIA) column examples are compiled by the following commands in their respective subfolders

```bash
make test_mecbmcolumn_tpfa
make test_mecbmretardationcolumn_tpfa
make test_mecbmbioretardationcolumn_tpfa
```

The fully implicit (GIA) column examplesare run via (e.g. Case11 Coal++ is identified with Coal11)
```bash
cd emmert2020c/build-cmake/appl/mecbm/column/implicit/
./test_mecbmbatchtest_mecbmcolumn_tpfa test_mecbmcolumnCoal11LessBiofilm.input
```

All sensitivies are analysed with the software tool SALib.

__Sequential column study__

The sequential column study examples are in the folder
```
emmert2020c/build-cmake/appl/mecbm/column/sequential/SNI/
emmert2020c/build-cmake/appl/mecbm/column/sequential/SSNIA_RTR/
emmert2020c/build-cmake/appl/mecbm/column/sequential/SSNIA_TRT/
```

The sequential column study examples are compiled by the following commands in their respective subfolders

```bash
make test_mecbmcolumn_tpfa
```


The sequential column study examples are run via (e.g. sequential non-iterative (SNI))
```bash
cd emmert2020c/build-cmake/appl/mecbm/column/sequential/SNI/
./test_mecbmcolumn_tpfa input
```



