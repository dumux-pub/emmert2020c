#!/bin/sh

# dune-common
# releases/2.7 # 20a8172d68a26d3f87457c3e2f2a7a7e24f609db # 2020-05-18 10:07:11 +0000 # Dominic Kempf
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.7
git reset --hard 20a8172d68a26d3f87457c3e2f2a7a7e24f609db
cd ..

# dune-foamgrid
# releases/2.7 # d49187be4940227c945ced02f8457ccc9d47536a # 2020-01-06 15:36:03 +0000 # Timo Koch
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid
cd dune-foamgrid
git checkout releases/2.7
git reset --hard d49187be4940227c945ced02f8457ccc9d47536a
cd ..

# dune-geometry
# releases/2.7 # fb75493b000410cbd157927f80c68ec9e86c5553 # 2020-04-29 04:38:04 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.7
git reset --hard fb75493b000410cbd157927f80c68ec9e86c5553
cd ..

# dune-grid
# releases/2.7 # 550c58dc9c75ebf9ff2d640bd5ccbfd100714ffe # 2020-04-29 05:11:51 +0000 # Oliver Sander
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.7
git reset --hard 550c58dc9c75ebf9ff2d640bd5ccbfd100714ffe
cd ..

# dune-istl
# releases/2.7 # 399ed5996e0b15c2038290810ad0f07d5f1e4c30 # 2020-05-14 20:58:36 +0000 # Christoph Grüninger
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.7
git reset --hard 399ed5996e0b15c2038290810ad0f07d5f1e4c30
cd ..

# dune-localfunctions
# releases/2.7 # ad588f9599b12b1615e760ae0106594f2059ab3e # 2020-05-15 07:48:41 +0000 # Dominic Kempf
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.7
git reset --hard ad588f9599b12b1615e760ae0106594f2059ab3e
cd ..

# dumux
# releases/3.2 # 398f8fb841e1ace6132f265b482df1147ad46b1b # 2020-05-28 11:19:50 +0200 # Ned Coltman
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout releases/3.2
git reset --hard 398f8fb841e1ace6132f265b482df1147ad46b1b
cd ..

# the pub-module containing the code for the examples
git clone https://git.iws.uni-stuttgart.de/dumux-pub/emmert2020c

# configure project
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
